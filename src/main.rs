// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use anyhow::Result;
use mwbot::parsoid::prelude::*;
use mwbot::Bot;
use mwseaql::sea_query::{expr::Expr, MysqlQueryBuilder, Query};
use mwseaql::{
    page_assessments::{PageAssessments, PageAssessmentsProjects},
    Page,
};
use mysql_async::prelude::*;
use mysql_async::{Conn, Pool};
use rocket::response::Debug;
use rocket::State;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use std::collections::HashSet;
use tokio::sync::OnceCell;

type RouteResult<T> = std::result::Result<T, Debug<anyhow::Error>>;

static BOT: OnceCell<Bot> = OnceCell::const_new();

async fn query_projects(conn: &mut Conn) -> Result<Vec<String>> {
    let query = Query::select()
        .from(PageAssessmentsProjects::Table)
        .column(PageAssessmentsProjects::ProjectTitle)
        .and_where(Expr::col(PageAssessmentsProjects::ParentId).is_null())
        .to_string(MysqlQueryBuilder);
    let mut rows: Vec<String> = conn.query(query).await?;
    rows.sort();
    Ok(rows)
}

async fn query_titles(
    conn: &mut Conn,
    titles: Vec<String>,
    project: &str,
) -> Result<HashSet<String>> {
    let query = Query::select()
        .from(Page::Table)
        .column(Page::Title)
        .inner_join(
            PageAssessments::Table,
            Expr::col(Page::Id).equals(PageAssessments::PageId),
        )
        .inner_join(
            PageAssessmentsProjects::Table,
            Expr::col(PageAssessments::ProjectId)
                .equals(PageAssessmentsProjects::ProjectId),
        )
        .and_where(Expr::col(Page::Namespace).eq(0))
        .and_where(Expr::col(Page::Title).is_in(titles))
        .and_where(Expr::col(PageAssessmentsProjects::ProjectTitle).eq(project))
        .to_string(MysqlQueryBuilder);
    // println!("{}", &query);
    let rows: Vec<String> = conn.query(query).await?;
    Ok(rows.into_iter().collect())
}

#[derive(Serialize)]
struct IndexTemplate {
    projects: Vec<String>,
}

#[get("/")]
async fn index(pool: &State<Pool>) -> RouteResult<Template> {
    Ok(Template::render("index", build_index(pool).await?))
}

async fn build_index(pool: &Pool) -> Result<IndexTemplate> {
    let mut conn = pool.get_conn().await?;
    let projects = query_projects(&mut conn).await?;
    Ok(IndexTemplate { projects })
}

#[derive(Serialize)]
struct ItemsTemplate {
    projects: Vec<String>,
    project: String,
    items: Vec<(String, Vec<String>)>,
}

#[get("/items?<project>")]
async fn items(project: String, pool: &State<Pool>) -> RouteResult<Template> {
    Ok(Template::render("items", build_items(project, pool).await?))
}

async fn build_items(project: String, pool: &Pool) -> Result<ItemsTemplate> {
    let mut conn = pool.get_conn().await?;
    let bot = BOT
        .get_or_init(|| async { Bot::from_default_config().await.unwrap() })
        .await;
    let page = bot.page("Wikipedia:Main Page queue")?;
    let html = page.html().await?;
    let html2 = html.clone();
    let all_links: Vec<_> = {
        let mut all_links = HashSet::new();
        let html = html.into_mutable();
        for link in html.filter_links() {
            let target = match bot.page(&link.target()) {
                Ok(page) => page,
                Err(_) => {
                    continue;
                }
            };
            if target.namespace() == 0 {
                all_links.insert(link.target().replace(' ', "_"));
            }
        }
        all_links.into_iter().collect()
    };
    let matching_titles = query_titles(&mut conn, all_links, &project).await?;

    let items = {
        let mut items = vec![];
        let html = html2.into_mutable();
        for section in html.iter_sections() {
            let heading = match section.heading() {
                Some(heading) => heading.text_contents(),
                None => {
                    continue;
                }
            };
            let mut section_links = vec![];
            // Look for bolded links only
            for bold in section.select("b") {
                let links = bold.as_generic().unwrap().filter_links();
                for link in links {
                    let title = link.target();
                    let target = bot.page(&title)?;
                    if target.namespace() == 0 {
                        let dbkey = link.target().replace(' ', "_");
                        if matching_titles.contains(&dbkey)
                            && !section_links.contains(&title)
                        {
                            section_links.push(title);
                        }
                    }
                }
            }
            if !section_links.is_empty() {
                items.push((heading, section_links));
            }
        }
        items
    };
    let projects = query_projects(&mut conn).await?;
    Ok(ItemsTemplate {
        projects,
        items,
        project,
    })
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .manage(Pool::new(
            toolforge::connection_info!("enwiki", WEB)
                .expect("loading db config failed")
                .to_string()
                .as_str(),
        ))
        .mount("/", routes![index, items])
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}
